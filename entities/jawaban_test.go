package entities

type JawabanTest struct {
	IdJawabanTest string
	IdPengguna    string
	Test          Test
	Nomer         int
	Pengguna      Pengguna `gorm:"foreignKey:IdPengguna;refrences:IdPengguna"`
}

func (u *JawabanTest) TableName() string {
	return "jawaban_test"
}
