package entities

import "time"

type Test struct {
	IdTest                 string
	IdPengguna             string
	PaketSoal              PaketSoal
	WaktuMulaiPengerjaan   time.Time
	WaktuSelesaiPengerjaan time.Time
	Status                 int
	Pengguna               Pengguna `gorm:"foreignKey:IdPengguna;refrences:IdPengguna"`
}

func (u *Test) TableName() string {
	return "test"
}
