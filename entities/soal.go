package entities

import "time"

type Soal struct {
	IdSoal     string `gorm:"primary_key;column:id_soal"`
	IdTipeSoal int
	// Content            string
	Text               string
	Jawaban            string
	AlternatifJawaban1 string
	AlternatifJawaban2 string
	AlternatifJawaban3 string
	AlternatifJawaban4 string
	AlternatifJawaban5 string
	CreatedAt          time.Time
	UpdatedAt          time.Time
	PilihanSoal        []PilihanSoal `gorm:"foreignKey:IdSoal;refrences:IdSoal"`
}

func (u *Soal) TableName() string {
	return "soal"
}
