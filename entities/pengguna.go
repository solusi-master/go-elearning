package entities

type Pengguna struct {
	IdPengguna string
	NmPengguna string
	Username   string
	Password   string
	Siswa      Siswa `gorm:"foreignKey:IdPengguna;refrences:IdPengguna"`
}

func (u *Pengguna) TableName() string {
	return "pengguna"
}
