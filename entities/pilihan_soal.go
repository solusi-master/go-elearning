package entities

import "time"

type PilihanSoal struct {
	IdPilihanSoal string `gorm:"primary_key;column:id_pilihan_soal"`
	IdSoal        string
	NumberOption  string
	Text          string
	// Content       string
	Correct   string
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (u *PilihanSoal) TableName() string {
	return "pilihan_soal"
}
