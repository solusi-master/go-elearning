package entities

import "time"

type PaketSoal struct {
	IdPaketSoal     string `gorm:"primary_key;column:id_paket_soal"`
	Text            string
	WaktuMulai      string
	WaktuSelesai    string
	WaktuPengerjaan int
	Status          int
	CreatedAt       time.Time
	UpdatedAt       time.Time
	DetailPaketSoal []DetailPaketSoal `gorm:"foreignKey:IdPaketSoal;refrences:IdPaketSoal"`
	// PaketSoalKelas  PaketSoalKelas    `gorm:"foreignKey:IdPaketSoal;refrences:IdPaketSoal"`
}

func (u *PaketSoal) TableName() string {
	return "paket_soal"
}
