package entities

type DetailPaketSoal struct {
	IdDetailPaketSoal string
	IdPaketSoal       string
	IdSoal            string
	Soal              Soal `gorm:"foreignKey:IdSoal;refrences:IdSoal"`
}

func (u *DetailPaketSoal) TableName() string {
	return "detail_paket_soal"
}
