package entities

type Siswa struct {
	IdSiswa    string `gorm:"primary_key;column:id_siswa"`
	IdPengguna string
	IdKelas    string
}

func (u *Siswa) TableName() string {
	return "siswa"
}
