package entities

type PaketSoalKelas struct {
	IdPaketSoalKelas string
	IdPaketSoal      string
	IdKelas          string
	Soal             Soal `gorm:"foreignKey:IdSoal;refrences:IdSoal"`
}

func (u *PaketSoalKelas) TableName() string {
	return "paket_soal_kelas"
}
