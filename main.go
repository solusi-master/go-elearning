package main

import (
	"fmt"
	authcontroller "go-elearning/controllers"
	paketsoalcontroller "go-elearning/controllers"
	"net/http"
)

func main() {
	// config.ConnectDB()

	http.HandleFunc("/", authcontroller.Index)
	http.HandleFunc("/login", authcontroller.Login)
	http.HandleFunc("/logout", authcontroller.Logout)
	http.HandleFunc("/test", paketsoalcontroller.IndexTest)

	fmt.Println("Server jalan di: http://localhost:3000")
	http.ListenAndServe(":3000", nil)
}
