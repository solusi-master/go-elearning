package controllers

import (
	"errors"
	"go-elearning/config"
	"go-elearning/entities"
	"go-elearning/libraries"
	"html/template"
	"net/http"

	"golang.org/x/crypto/bcrypt"
)

type UserInput struct {
	Username string `validate:"required"`
	Password string `validate:"required"`
}

var db = config.OpenConnection()

func Index(w http.ResponseWriter, r *http.Request) {

	session, _ := config.Store.Get(r, config.SESSION_ID)

	if len(session.Values) == 0 {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
	} else {

		if session.Values["loggedIn"] != true {
			http.Redirect(w, r, "/login", http.StatusSeeOther)
		} else {
			idKelas, _ := session.Values["id_kelas"].(string)
			var list_paket_soal []entities.PaketSoal
			db.Model(&entities.PaketSoal{}).Joins("LEFT JOIN paket_soal_kelas ON paket_soal.id_paket_soal = paket_soal_kelas.id_paket_soal").Where("paket_soal_kelas.id_kelas = ?", idKelas).Find(&list_paket_soal)

			data := map[string]interface{}{
				"nm_pengguna":     session.Values["nm_pengguna"],
				"list_paket_soal": list_paket_soal,
			}

			temp, _ := template.ParseFiles("views/index.html")
			temp.Execute(w, data)
		}

	}
}

func Login(w http.ResponseWriter, r *http.Request) {

	if r.Method == http.MethodGet {
		temp, _ := template.ParseFiles("views/login.html")
		temp.Execute(w, nil)
	} else if r.Method == http.MethodPost {
		// proses login
		r.ParseForm()
		UserInput := &UserInput{
			Username: r.Form.Get("username"),
			Password: r.Form.Get("password"),
		}

		errorMessages := libraries.Struct(UserInput)

		if errorMessages != nil {

			data := map[string]interface{}{
				"validation": errorMessages,
			}

			temp, _ := template.ParseFiles("views/login.html")
			temp.Execute(w, data)

		} else {

			var pengguna entities.Pengguna
			var message error
			db.Joins("Siswa").Take(&pengguna, "username = ?", UserInput.Username)

			if pengguna.Username == "" {
				message = errors.New("Username atau Password salah!")
			} else {
				// pengecekan password
				errPassword := bcrypt.CompareHashAndPassword([]byte(pengguna.Password), []byte(UserInput.Password))
				if errPassword != nil {
					message = errors.New("Username atau Password salah!")
				}
			}

			if message != nil {

				data := map[string]interface{}{
					"error": message,
				}

				temp, _ := template.ParseFiles("views/login.html")
				temp.Execute(w, data)
			} else {
				// set session
				session, _ := config.Store.Get(r, config.SESSION_ID)

				session.Values["loggedIn"] = true
				session.Values["username"] = pengguna.Username
				session.Values["id_kelas"] = pengguna.Siswa.IdKelas
				session.Values["nm_pengguna"] = pengguna.NmPengguna

				session.Save(r, w)

				http.Redirect(w, r, "/", http.StatusSeeOther)
			}
		}

	}

}

func Logout(w http.ResponseWriter, r *http.Request) {
	session, _ := config.Store.Get(r, config.SESSION_ID)
	// delete session
	session.Options.MaxAge = -1
	session.Save(r, w)

	http.Redirect(w, r, "/login", http.StatusSeeOther)
}
