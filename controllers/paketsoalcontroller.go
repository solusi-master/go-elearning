package controllers

import (
	"go-elearning/config"
	"go-elearning/entities"
	"html/template"
	"net/http"
	"strconv"
)

func IndexTest(w http.ResponseWriter, r *http.Request) {
	var db = config.OpenConnection()
	if r.Method == "GET" {
		temp, err := template.ParseFiles("views/test.html")
		if err != nil {
			panic(err)
		}

		idPaketSoal := r.URL.Query().Get("id")
		no := r.URL.Query().Get("no")

		if no == "" {
			no = "1"
		}

		nomer, err := strconv.Atoi(no)

		if err != nil {
			panic(err)
		}

		var paketsoal entities.PaketSoal

		err = db.Take(&paketsoal, "id_paket_soal = ?", idPaketSoal).Error
		if err != nil {
			panic(err)
		}
		var list_detailpaketsoal []entities.DetailPaketSoal
		err = db.Model(&entities.DetailPaketSoal{}).Joins("Soal").Order("RAND()").Find(&list_detailpaketsoal, "id_paket_soal = ?", idPaketSoal).Error
		// db.Debug().Model(&entities.DetailPaketSoal{}).Preload("Soal").Order("RAND()").Find(&list_detailpaketsoal, "id_paket_soal = ?", idString)

		if err != nil {
			panic(err)
		}

		session, _ := config.Store.Get(r, idPaketSoal+"soal")
		session.Options.MaxAge = -1
		session.Save(r, w)

		session, _ = config.Store.Get(r, idPaketSoal+"tipe_soal")
		session.Options.MaxAge = -1
		session.Save(r, w)

		session, _ = config.Store.Get(r, idPaketSoal+"pilihan_soal")
		session.Options.MaxAge = -1
		session.Save(r, w)

		session, _ = config.Store.Get(r, idPaketSoal+"pilihan_soal_correct")
		session.Options.MaxAge = -1
		session.Save(r, w)

		session_text, _ := config.Store.Get(r, idPaketSoal+"soal")
		session_id_tipe_soal, _ := config.Store.Get(r, idPaketSoal+"tipe_soal")
		session_pilihan_soal, _ := config.Store.Get(r, idPaketSoal+"pilihan_soal")
		session_pilihan_soal_correct, _ := config.Store.Get(r, idPaketSoal+"pilihan_soal_correct")

		p_soal := []string{}
		p_correct := []string{}

		for i := 0; i < len(list_detailpaketsoal); i++ {
			session_text.Values[i+1] = list_detailpaketsoal[i].Soal.Text
			session_id_tipe_soal.Values[i+1] = list_detailpaketsoal[i].Soal.IdTipeSoal
			if session_id_tipe_soal.Values[i+1] == 1 {
				var pilihansoal []entities.PilihanSoal
				err = db.Model(&entities.PilihanSoal{}).Find(&pilihansoal, "id_soal = ?", list_detailpaketsoal[i].IdSoal).Error
				if err != nil {
					panic(err)
				}
				p_soal = []string{pilihansoal[0].Text, pilihansoal[1].Text, pilihansoal[2].Text, pilihansoal[3].Text, pilihansoal[4].Text}
				p_correct = []string{pilihansoal[0].Correct, pilihansoal[1].Correct, pilihansoal[2].Correct, pilihansoal[3].Correct, pilihansoal[4].Correct}
				session_pilihan_soal.Values[i+1] = p_soal
				session_pilihan_soal_correct.Values[i+1] = p_correct
			}

		}
		session_text.Save(r, w)
		session_id_tipe_soal.Save(r, w)
		session_pilihan_soal.Save(r, w)
		session_pilihan_soal_correct.Save(r, w)

		// println(no)
		session_soal, _ := config.Store.Get(r, idPaketSoal+"soal")
		data_soal := session_soal.Values[nomer]

		pilihan_soal, _ := config.Store.Get(r, idPaketSoal+"pilihan_soal")
		data_pilihan_soal := pilihan_soal.Values[nomer]
		list_pilihan_soal, _ := data_pilihan_soal.([]string)

		session_tipe_soal, _ := config.Store.Get(r, idPaketSoal+"tipe_soal")
		tipe_soal := session_tipe_soal.Values[nomer]

		data := map[string]any{
			"data_soal":         data_soal,
			"list_pilihan_soal": list_pilihan_soal,
			"tipe_soal":         tipe_soal,
			"idPaketSoal":       idPaketSoal,
			"no":                nomer + 1,
		}

		temp.Execute(w, data)

	}

}
